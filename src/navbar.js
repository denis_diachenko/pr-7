import React from "react";

import { NavLink } from "react-router-dom";

const Navbar = () => (
  <nav className="navbar">
    <h1>
      <NavLink exact to="/">
        Angular Routing
      </NavLink>
    </h1>
    <ul>
      <li>
        <NavLink exact to="/">
          Home
        </NavLink>
      </li>
      <li>
        <NavLink to="/posts">Posts</NavLink>
      </li>
      <li>
        <NavLink to="/about">About</NavLink>
      </li>
      <li>
        <button className="btn">Login</button>
      </li>
      <li>
        <button className="btn">Logout</button>
      </li>
    </ul>
  </nav>
);

export default Navbar;
