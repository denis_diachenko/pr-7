import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./home";
import About from "./about";
import Posts from "./posts";
import Post from "./post";
import Navbar from "./navbar"

const App = () => (
  <>
    <Navbar />
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/about" exact component={About} />
      <Route path="/posts" exact component={Posts} />
      <Route path="/posts/:id" exact component={Post} />
    </Switch>
  </>
);

export default App;
