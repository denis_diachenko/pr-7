import React from "react";
import { withRouter } from "react-router-dom";

const Home = ({ history }) => (
  <div className="container">
    <h1>Home page</h1>
    <button className="btn" onClick={() => history.push("/posts")}>
      Go to posts
    </button>
  </div>
);

export default withRouter(Home);
