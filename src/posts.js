import React from "react";
import { Link } from "react-router-dom";

import posts from "./static/posts.json";

const Posts = () => (
  <div className="container card">
    <h1>Posts page</h1>
    {posts.map((p) => (
      <div key={p.id} className="card">
        <Link to={`/posts/${p.id}`}>
          #{p.id} - {p.title}
        </Link>
      </div>
    ))}
  </div>
);

export default Posts;
