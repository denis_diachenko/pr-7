import React from 'react';

import posts from './static/posts.json';

const Post = ({ history, match: { params: { id } } }) => {
  const post = posts.find((p) => p.id === id);
  return (
    <div className="container">
      <button className="btn" onClick={() => history.goBack()}>
        Back
      </button>
      <div className="card">
        <h1>
          Post {post.id} - {post.title}
        </h1>
        <div>Post {post.text}</div>
      </div>
      <button
        className="btn btn-danger"
        onClick={() => alert('Oooops')}
      >
        Load 4 posts
      </button>
    </div>
  );
}

export default Post;
